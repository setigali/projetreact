import React, { useState } from "react";

export default function ModifyUser() {
  
  return (
      <div class="createuser">

<form>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="admin" value="option1"></input>
  <label class="form-check-label" for="inlineCheckbox1">Admin</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="formateur" value="option2"></input>
  <label class="form-check-label" for="inlineCheckbox2">Formateur</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" id="stagiaire" value="option3"></input>
  <label class="form-check-label" for="inlineCheckbox3">Stagiaire</label>
</div>
  <div class="row">
    <div class="col">
      <input type="text" class="form-control" placeholder="Nom"></input>
    </div>
    <div class="col">
      <input type="text" class="form-control" placeholder="Last name"></input>
    </div>
 
  </div>
  <br></br>
  <div class="row">
    <div class="col">
      <input type="text" class="form-control" placeholder="Prenom"></input>
    </div>
    <div class="col">
      <input type="text" class="form-control" placeholder="Adresse"></input>
    </div>
    
  </div>

  <br></br>
  <div class="row">
    <div class="col">
      <input type="text" class="form-control" placeholder="Email"></input>
    </div>
    <div class="col">
      <input type="text" class="form-control" placeholder="Login"></input>
    </div>
    
  </div>
  <br></br>
  <div class="row">
    <div class="col">
      <input type="text" class="form-control" placeholder="Telephone"></input>
    </div>
    <div class="col">
      <input type="text" class="form-control" placeholder="Mot de passe"></input>
    </div>
    
  </div>
  <br></br>
  <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Valider</button>
     
      <button type="button" class="btn btn-danger" >Quitter</button>
    </div>
    
</form>
</div>
  );
}
