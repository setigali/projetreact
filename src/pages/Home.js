import React, { useState } from "react";

export default function Home() {
  const [email, setEmail] = useState("alex.alex@gmail.com");

  const handleSubmit = (evt) => {
    evt.preventDefault();
    alert(`Submitting Email ${email}`)
  }
  return (
      <div class="home">

        <form class="text-center border border-light" action="#!">

          <p class="h4 mb-4">Authentification</p>

          <input type="text" id="login" class="form-control mb-4" placeholder="Login"></input>

          <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Mot de passe"></input>

          <div class="d-flex justify-content-around">
            <div>

            </div>

          </div>

          <button class="btn btn-info btn-block my-4" type="submit">Valider</button>

          <p>Vous n'avez pas de compte?
        <a href="">creer un compte</a>
          </p>

        </form>

      </div>
  );
}
