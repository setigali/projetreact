import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import Home from "./pages/Home";
import About from "./pages/ListerSalle";
import Contact from "./pages/Contact";
import CreateUser from "./pages/createUser";
import ModifyUser from "./pages/modifyUser";

function App() {
  return (
    <Router>
    <Header />
      <div>
        <Switch>
          <Route path="/ListerSalle">
            <About />
          </Route>
          <Route path="/contact">
            <Contact />
          </Route>
          <Route path="/createUser" exact component={CreateUser}>
            <CreateUser />
          </Route>
         
          <Route path="/modifyUser" exact component={ModifyUser}>
            <ModifyUser />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
