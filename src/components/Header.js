import React from "react";
import {
    Link
  } from "react-router-dom";

export default function Header() {
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="#">
        Afpa
      </a>
      <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">
            <Link to="/">Home</Link> <span class="sr-only">(current)</span>
            </a>
          </li>
        
          
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/contact">Contact</Link>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/createuser">Créer utilisateur</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/modifyuser">Modifier utilisateur</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/contact">Lister utilisateur</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/contact">Creer salle</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/contact">Modifier salle</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/ListerSalle">Lister salle</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/contact">Reserver salle</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/contact">Creer type materiel</Link>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
            <Link to="/contact">Creer batiment</Link>
            </a>
          </li>

        </ul>
      </div>
    </nav>
  );
}
